#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=
MODE=Release

if [ ! -d $basedir/build ]
then
	mkdir $basedir/build
fi

read -p "**********************************************************
Welcome to NGE Server v2.3 by Erusman! 
This script will pull latest src / dsrc from my bitbucket repo.
**********************************************************
Do you want to pull/update git? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	# update main repo
        cd /home/swg/swg-main
	git pull
fi

read -p "**********************************************************
This script will compile the src to binaries.
New bins will be located in /home/swg/swg-main/build/bin
**********************************************************
Do you want to build the server now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
  
        unset ORACLE_HOME;
        unset ORACLE_SID;
        unset JAVA_HOME;   
        export ORACLE_HOME=/usr/lib/oracle/12.1/client;
        export JAVA_HOME=/usr/java;
        export ORACLE_SID=swg;
        rm -rf /home/swg/swg-main/build
        mkdir /home/swg/swg-main/build
	mkdir /home/swg/swg-main/build/bin
	cd $basedir/build

	# prefer clang
	if [[ $MODE =~ ^(Release) ]]; then
		if type clang-3.8 &> /dev/null; then
			export CC=clang-3.8
			export CXX=clang++-3.8
		else
			if type clang &> /dev/null; then
				export CC=clang
				export CXX=clang++
			fi
		fi	
	fi

	if [ $(arch) == "x86_64" ]; then
        	export LDFLAGS=-L/usr/lib32
		export CMAKE_PREFIX_PATH="/usr/lib32:/lib32:/usr/lib/i386-linux-gnu:/usr/include/i386-linux-gnu"

		cmake -DCMAKE_C_FLAGS=-m32 \
		-DCMAKE_CXX_FLAGS=-m32 \
		-DCMAKE_EXE_LINKER_FLAGS=-m32 \
		-DCMAKE_MODULE_LINKER_FLAGS=-m32 \
		-DCMAKE_SHARED_LINKER_FLAGS=-m32 \
		-DCMAKE_BUILD_TYPE=$MODE \
		$basedir/src
	else
		cmake $basedir/src -DCMAKE_BUILD_TYPE=$MODE
	fi

	make -j$(nproc)

	cd $basedir
fi

read -p "**********************************************************
This script will add your VM's IP to NGE Server configs  
New configs will be built in /home/swg/swg-main/exe/linux 
**********************************************************
Do you want to build the config environment now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then

	# Prompt for configuration environment.
	read -p "Configure environment (local, live, tc, design)? Default is 'local' " config_env

	# Make sure the configuration environment exists.
	if [ ! -d $basedir/configs/$config_env ]; then
		echo "Invalid configuration environment."
		exit
	fi

        
	echo "Enter your IP address (LAN for port forwarding or internal, outside IP for DMZ) "
	read HOSTIP

	echo "Enter the DSN for the database connection. 127.0.0.1 or IP of VM "
	read DBSERVICES

	echo "Enter the database username. i.e. swg "
	read DBUSERNAMES

	echo "Enter the database password i.e swg "
	read DBPASSWORDS

	echo "Enter a name for the galaxy cluster you used to import swg database. "
	read CLUSTERNAME

	if [ -d $basedir/exe ]; then
		rm -rf $basedir/exe
	fi

	mkdir -p $basedir/exe/linux/logs
	mkdir -p $basedir/exe/shared

	ln -s $basedir/build/bin $basedir/exe/linux/bin

	cp -u $basedir/configs/$config_env/linux/* $basedir/exe/linux
	cp -u $basedir/configs/$config_env/shared/* $basedir/exe/shared

	for filename in $(find $basedir/exe -name '*.cfg'); do
		sed -i -e "s@DBSERVICES@$DBSERVICES@g" -e "s@DBUSERNAMES@$DBUSERNAMES@g" -e "s@DBPASSWORDS@$DBPASSWORDS@g" -e "s@CLUSTERNAME@$CLUSTERNAME@g" -e "s@HOSTIP@$HOSTIP@g" $filename
	done

	#
	# Generate other config files if their template exists.
	#

		# Generate at least 1 node that is the /etc/hosts IP.
		$basedir/utils/build_node_list.sh
fi

read -p "**********************************************************
This script will compile the /dsrc. Java scripts, datatables etc.
**********************************************************
Do you want to build the scripts now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	read -p "Do you wanna use multicore scripts or the safe option? 
Recommended you use safe for this VM operation.(multi/safe) " response
         response=${response,,}
        if [[ $response =~ ^(multi|m| ) ]]; then
          $basedir/utils/build_java_multi.sh
          $basedir/utils/build_miff.sh
          $basedir/utils/build_tab_multi.sh
          $basedir/utils/build_tpf_multi.sh
        else
          $basedir/utils/build_java.sh
          $basedir/utils/build_miff.sh
          $basedir/utils/build_tab.sh
          $basedir/utils/build_tpf.sh
        fi

	$basedir/utils/build_object_template_crc_string_tables.py
	$basedir/utils/build_quest_crc_string_tables.py

	PATH=$oldPATH
fi

read -p "***********************************************
This script will pull latest /clientdata from bitbucket repo
*********************************************** 
Do you want to pull/update git? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
    # update main repo /clientdata
        cd /home/swg/clientdata
	git pull
        cd /home/swg/swg-main

fi	

read -p "***********************************************
This script will merge latest /clientdata to your server.
WARNING All previous /cliendata on your server will be deleted!
***********************************************
Do you want to copy over /cliendata to /data? (y/n) " response
response=${response,,}

        if [[ $response =~ ^(yes|y| ) ]]; then
        cd /home/swg/swg-main/data/sku.0/sys.client
        rm -rf compiled 
        cd /home/swg/swg-main/data/sku.0/sys.client
        mkdir compiled
        cd /home/swg/swg-main/data/sku.0/sys.client/compiled
        mkdir game
        cp -a /home/swg/clientdata/. /home/swg/swg-main/data/sku.0/sys.client/compiled/game/
        cd /home/swg/swg-main

fi

read -p "***********************************************
This script will rebuild your Chat database
*********************************************** 
WARNING ALL previous chat user data will be deleted!
This will include their emails, and all chat data!
Do you want to rebuild Chat database? (y/n) " response
response=${response,,}
         if [[ $response =~ ^(yes|y| ) ]]; then
         cd /home/swg/swg-main/chat/
         rm chat.db
         cd /home/swg/swg-main/chat/default_db
         cp chat.db /home/swg/swg-main/chat
         cd /home/swg/swg-main

fi

read -p "***********************************************
This script will import the SWG database to Oracle 11g R2
*********************************************** 
Do you want to import the database to Oracle? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then

	cd /home/swg/swg-main/opt/database/build/linux;
        unset ORACLE_HOME;
        unset ORACLE_SID;
        unset JAVA_HOME;
        export JAVA_HOME=/usr/java;
        export ORACLE_HOME=/u01/app/product/11.2.0/dbhome_1;
        export ORACLE_SID=swg;
        export PATH=$ORACLE_HOME/bin:$PATH;

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection. i.e //127.0.0.1/swg "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username. i.e swg "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password. i.e swg "
		read DBPASSWORD
	fi

	./database_update.pl --username=$DBUSERNAME --password=$DBPASSWORD --service=$DBSERVICE --goldusername=$DBUSERNAME --loginusername=$DBUSERNAME --createnewcluster --packages

	echo "Loading template list from object template crc string table "
	perl $basedir/opt/database/templates/processTemplateList.pl < $basedir/opt/object_template_crc_string_table.tab > $basedir/build/templates.sql
	sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @$basedir/build/templates.sql > $basedir/build/templates.out
	cd $basedir
fi
echo "Congratulations build_linux script is complete!"
