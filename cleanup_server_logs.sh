#!/bin/bash
read -p "***********************************************
This script will cleanup your Servers logs
*********************************************** 
Do you want to cleanup Server logs now? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
cd /home/swg/swg-main/exe/linux
rm -rf logs
mkdir logs
fi
echo "Cleaup Server logs script is complete!"